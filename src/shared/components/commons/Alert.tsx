import React, { useState, useEffect } from "react";
import { css } from "@emotion/core";

interface IAlertProps {
  title: any;
  text?: any;
  type?: string;
  duration?: number;
}

export default function Alert({
  title,
  text,
  type,
  duration = 3
}: IAlertProps) {
  let bg = "bg-green-500";
  let borderbg = "border-green-500";

  const [show, setShow] = useState(true);

  switch (type) {
    case "success":
      bg = "bg-green-500";
      borderbg = "border-green-900";
      break;
    case "error":
      bg = "bg-red-500";
      borderbg = "border-red-900";
      break;
    case "warning":
      bg = "bg-orange-500";
      borderbg = "border-orange-900";
      break;
  }

  useEffect(() => {
    setShow(true);
    const timeShow = setTimeout(() => {
      setShow(false);
    }, duration * 1000);
    return () => {
      clearTimeout(timeShow);
    };
  }, [bg, duration]);

  let AlertComponent = null;

  if (show) {
    AlertComponent = (
      <div
        className={`${bg} rounded text-white  px-5 py-3 border  ${borderbg}`}
        css={css`
          opacity: 1;
          transition: opacity 1s ease-in;
        `}
      >
        <h1 className=" font-bold text-xl uppercase">{title}</h1>
        <p className=" ">{text}</p>
      </div>
    );
  }

  return AlertComponent;
}
