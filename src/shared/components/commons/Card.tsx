import React from "react";

interface CardProps {
  title?: any
  subtitle?:any;
  children: any;
  img?: string;
}

export default function Card(props: CardProps) {
  const header = props.title ? (
    <div className="mb-4 text-center p-3 mb-1">
      <p className="uppercase font-bold">{props.title}</p> 
      {
        props.subtitle
        ? <p>{props.subtitle}</p>
        :null
      }
      <hr />
    </div>
  ) : null;

  return (
    <div className="shadow-lg p-10">
      {header}
      <div className="w-full">{props.children}</div>
    </div>
  );
}
