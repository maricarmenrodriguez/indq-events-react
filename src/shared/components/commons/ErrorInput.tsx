import React from "react";

interface IErrorInputProps {
  message: any;
}

export default function ErrorInput({ message }: IErrorInputProps) {
  return (
    <span className="text-red-700 hover:text-red-800 uppercase text-sm ml-2">
      * {message}
    </span>
  );
}
