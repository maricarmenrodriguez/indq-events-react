import React, { Fragment } from "react";
import { UserLoggedInConsumer } from "../../../data/userLoggedIn/UserLoggedIn";
import {
  IntlConsumer,
  aviableLocales
} from "../../../data/intl/context/IntelProvider";
import { FormattedMessage } from "react-intl";
//Router
import { Link } from "@reach/router";

const NavLink = props => (
  <Link
    {...props}
    getProps={({ isCurrent }) => {
      // the object returned here is passed to the
      // anchor element's props
      return {
        style: {
          borderBottom: isCurrent ?"solid white 1px" :null
        }
      };
    }}
  />
);


export default function Header() {
  return (
    <IntlConsumer>
      {values => {
        return (
          <UserLoggedInConsumer>
            {userLoggedInValues => {
              let contentnav: any;
              if (userLoggedInValues.id) {
                contentnav = (
                  <Fragment>
                    <li className="mr-6 text-gray-100 hover:text-blue-500 mt-1">
                      <NavLink to="/eventlist"><FormattedMessage id="header.event.list" /></NavLink>
                    </li>
                    <li className="mr-6 text-gray-100 hover:text-blue-500 mt-1">
                      <NavLink to="/registerevent">
                      <FormattedMessage id="header.event.add" />
                      </NavLink>
                    </li>
                    <li className="mr-6 text-gray-100 hover:text-blue-500 mt-1">
                      <NavLink
                        to="/login"
                        onClick={e => userLoggedInValues.logout()}
                      >
                        <FormattedMessage id="header.exit" />
                      </NavLink>
                    </li>
                  </Fragment>
                );
              } else {
                contentnav = (
                  <li className="mr-5 mt-1">
                    <Link to="/login">
                      <FormattedMessage id="header.message.login" />
                    </Link>
                  </li>
                );
              }
              return (
                <header className="w-full bg-gray-800 p-5 text-white">
                  <div className="container mx-auto flex ">
                    <div className="flex-1">
                      <h1 className="font-bold uppercase text-xl">
                        indq events
                      </h1>
                    </div>
                    <div className="flex-2">
                      <nav className="mr-auto font-bold">
                        <ul className="flex">
                          {contentnav}
                          <li className="hover:shadowed">
                            <select
                              name="languaje"
                              id="languaje"
                              className="p-2 rounded text-black"
                              onChange={e =>
                                values.changeLocale(e.target.value)
                              }
                            >
                              {aviableLocales.map(locale => (
                                <option key={locale.key} value={locale.key}>
                                  {locale.name}
                                </option>
                              ))}
                            </select>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </header>
              );
            }}
          </UserLoggedInConsumer>
        );
      }}
    </IntlConsumer>
  );
}
