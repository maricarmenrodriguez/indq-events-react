import React from "react";

export default function Footer() {
  return (
    <footer className="w-full text-center border-t border-t-2 border-t-gray-500 p-4 mt-10">
      contacto@indqtech.com 
    </footer>
  );
}
