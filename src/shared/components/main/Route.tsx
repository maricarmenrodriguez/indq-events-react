import React, { FunctionComponent } from "react";
//Router
import {  RouteComponentProps } from "@reach/router";

// type IPropsRoute = { pageComponent: JSX.Element } & RouteComponentProps;
// const Route: FunctionComponent<IPropsRoute> = props => props.pageComponent;

type Props = { component: FunctionComponent } & RouteComponentProps;

const Route: FunctionComponent<Props> = ({ component: Component, ...rest }) => (
  <Component {...rest} />
);


export default Route