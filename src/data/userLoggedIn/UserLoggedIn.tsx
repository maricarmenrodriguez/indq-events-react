"non-strinct";
import React, { createContext, Component } from "react";
import axios from "axios";
import global from "../../global.json";
import { navigate } from "@reach/router";

export interface IUserLogguedContext {
  id: string;
  token: string;
  firstName: string;
  lastName: string;
  login: Function;
  logout: Function;
}
interface IState {
  id: string;
  token: string;
  firstName: string;
  lastNane: string;
}

const UserLoggedInContext = createContext<IUserLogguedContext>({
  id: "",
  token: "",
  firstName: "",
  lastName: "",
  login: () => {},
  logout: () => {}
});

const defaultState = {
  id: "",
  token: "",
  firstName: "",
  lastNane: ""
};

export let userLoggedInData: IState;

class UserLoggedInProvider extends Component {
  url: string = `${global.api}users/login`;

  state = {
    ...defaultState
  };

  componentDidMount() {
    if (localStorage.getItem("userLoggedIn")) {
      let userLoggedIn = JSON.parse(localStorage.getItem("userLoggedIn") || "");
      this.setState(userLoggedIn);
      userLoggedInData = { ...this.state };
    } else {
      navigate(`/login`);
    }
  }

  login = async (email: string, password: string) => {
    try {
      let result = await axios.post(this.url, { email, password });
      let { id, token, firstName, lastName } = result.data;
      this.setState({
        id,
        token,
        firstName,
        lastName
      });
      localStorage.setItem("userLoggedIn", JSON.stringify(this.state));
      userLoggedInData = { ...this.state };
      console.log("Login data", this.state, userLoggedInData);
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  };

  logout = () => {
    userLoggedInData = undefined;
    this.setState(defaultState);
    localStorage.removeItem("userLoggedIn");
  };

  render() {
    return (
      <UserLoggedInContext.Provider
        value={{
          id: this.state.id,
          token: this.state.token,
          firstName: this.state.firstName,
          lastName: this.state.lastNane,
          login: this.login,
          logout: this.logout
        }}
      >
        {this.props.children}
      </UserLoggedInContext.Provider>
    );
  }
}

export const UserLoggedInConsumer = UserLoggedInContext.Consumer;
export default UserLoggedInProvider;
