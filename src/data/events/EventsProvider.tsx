import React, { Component } from "react";
import axios from "axios";
import { api } from "../../global.json";
import { IEvent } from "../../types";

interface props{
    token:string
}
interface State {
  evens: Array<IEvent>;
}
interface IEventsContext {
  events: Array<IEvent>;
  getEvents: Function;
  markAttended: Function;
  unmarkAttended: Function;
}

const EventsContext = React.createContext({});
export const  EventsContextConsumer = EventsContext.Consumer;

export default class EventsProvider extends Component<props, State> {

  private url: string = encodeURI(`${api}events`);
  private headers:Object = ({Authorization:this.props.token});

  private async getEvents(page:number=1, title:string='', localization:{lat:number, lng:number}=null) {

    let myurl = encodeURI(`${this.url}?page=${page}`)
    if(title) myurl+=`&title=${title}`;
    
    try {
        const result = await axios.get(this.url, this.headers);
        let { items } = result.data;
    } catch (error) {}
  }

  markAttended() {}

  unmarkAttended() {}

  render() {
    return <div></div>;
  }
}
