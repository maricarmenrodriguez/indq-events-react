/*
    Controlar los mensajes que se mestran en el intProvider y los locales desde aqui
*/
import React, { Component } from "react";
import Messages from "../messages/Messages";

export type TLocale = "es" | "en" | "fr";
export const aviableLocales = [
  { key: "es", name: "Español" },
  { key: "en", name: "English" },
  { key: "fr", name: "Francais" }
];

export const defaultLocale: TLocale = "es";

interface IValuesProvider {
  locale: TLocale;
  messages: Record<string, string>;
  changeLocale: Function;
}

interface IState {
  messages: Record<string, string>;
  locale: TLocale;
}

const IntlContext = React.createContext<IValuesProvider>({
  locale: defaultLocale,
  messages: Messages[defaultLocale],
  changeLocale: () => {}
});

class MyIntlProvider extends Component<{}, IState> {
  state = {
    messages: { ...Messages[defaultLocale] },
    locale: defaultLocale
  };

  changeLocale = (locale: TLocale) => {
    this.setState({
      locale: locale,
      messages: { ...Messages[locale] }
    });
  };

  render() {
    return (
      <IntlContext.Provider
        value={{
          locale: this.state.locale,
          messages: this.state.messages,
          changeLocale: this.changeLocale
        }}
      >
        {this.props.children}
      </IntlContext.Provider>
    );
  }
}

export default MyIntlProvider;
export const IntlConsumer = IntlContext.Consumer;
