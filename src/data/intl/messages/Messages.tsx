export default {
  es: {
    /* HEADER */
    "header.langueje.select": "Idioma",
    "header.event.add": "Agregar Evento",
    "header.event.list": "Listado de eventos",
    "header.exit": "salir",
    "header.message.login": "Para ver todas las opciones inicia sesion",

    /* LOGIN */
    "login.label.email": "Correo",
    "login.label.password": "Contraseña",
    "login.error.email": "El correo electronico ingresado es incorrecto",

    /* REGISTRO  */
    "register.title": "indq eventos",
    "register.firstName": "Nombre (s)",
    "register.lastName": "Apellido (s)",
    "register.email": "Correo",
    "register.gender": "Genero",
    "register.gender.male": "Masculino",
    "register.gender.female": "Femenino",
    "register.password": "Contraseña",
    "register.password_confirm": "Confirmar contraseña",
    "register.btn.confirm": "Confirmar",
    "register.btn.clean": "Limpiar",
    "register.btn.login": "¿Ya eres miembro?, Inicia sesión",
    //Errores
    "register.err.firstName": "El nombre no es valido",
    "register.err.firstName.required": "El nombre es obligatorio",
    "register.err.firstName.matches": "Solo se permiten letras",
    "register.err.lastName": "El apellido no es valido",
    "register.err.lastName.required": "el apellido es obligatorio",
    "register.err.email": "El correo no es valido",
    "register.err.email.required": "El correo es obligatorio",
    "regiter.err.password": "La contraseña no es valida",
    "register.err.password.required": "La contraseña es obligatoria",
    "register.err.password.min":
      "La contraseña debe tener un minimo de 8 caracteres",
    "register.err.password_confirm": "La confirmacion no es valida",
    //notify
    "register.notify.success.title": "Correcto",
    "register.notify.success.text": "Usuario guardado con exito",
    "register.notify.err.title": "Error",
    "register.notify.err.text": "Ya existe un usuario con ese correo"
  },
  en: {
    "header.langueje.select": "Language",
    "header.event.add": "Add Event",
    "header.event.list": "Event list",
    "header.exit": "exit",

    /* LOGIN */
    "login.label.email": "Mail",
    "login.label.password": "Password",
    "login.error.email": "The email entered is incorrect",

    /* REGISTRO */
    "register.title": "indq events",
    "register.firstName": "Name (s)",
    "register.lastName": "Surname (s)",
    "register.email": "Mail",
    "register.gender": "Gender",

    "register.gender.male": "Male",
    "register.gender.female": "Female",
    "register.password": "Password",
    "register.password_confirm": "Confirm password",
    "register.btn.confirm": "Confirm",
    "register.btn.clean": "Clean",
    "register.btn.login": "Already a member ?, Log in",
    "header.message.login": "To see all the options log in",
    //Mistakes
    "register.err.firstName": "The name is not valid",
    "register.err.firstName.required": "The name is mandatory",
    "register.err.firstName.matches": "Only letters are allowed",
    "register.err.lastName": "The last name is not valid",
    "register.err.lastName.required": "the last name is mandatory",
    "register.err.email": "Mail is not valid",
    "register.err.email.required": "Mail is mandatory",
    "regiter.err.password": "The password is not valid",
    "register.err.password.required": "Password is mandatory",
    "register.err.password.min":
      "The password must have a minimum of 8 characters",
    "register.err.password_confirm": "Confirmation is not valid",
    // notify
    "register.notify.success.title": "Correct",
    "register.notify.success.text": "User saved successfully",
    "register.notify.err.title": "Error",
    "register.notify.err.text": "A user with that email already exists"
  },
  fr: {
    "header.langueje.select": "Langue",
    "header.event.add": "Ajouter un événement",
    "header.event.list": "Liste d'événements",
    "header.exit": "exit",
    "header.message.login": "Pour voir toutes les options se connecter",

    /* LOGIN */
    "login.label.email": "Mail",
    "login.label.password": "Mot de passe",
    "login.error.email": "Le courrier électronique entré est incorrect",

    /* REGISTRO */
    "register.title": "indq events",
    "register.firstName": "Nom (s)",
    "register.lastName": "Nom (s) de famille",
    "register.email": "Mail",
    "register.gender": "Genre",
    "register.gender.male": "Homme",
    "register.gender.female": "Femme",
    "register.password": "Mot de passe",
    "register.password_confirm": "Confirmer le mot de passe",
    "register.btn.confirm": "Confirmer",
    "register.btn.clean": "Propre",
    "register.btn.login": "Déjà membre? Connectez-vous",
    // Erreurs
    "register.err.firstName": "Le nom n'est pas valide",
    "register.err.firstName.required": "Le nom est obligatoire",
    "register.err.firstName.matches": "Seules les lettres sont autorisées",
    "register.err.lastName": "Le nom de famille n'est pas valide",
    "register.err.lastName.required": "le nom de famille est obligatoire",
    "register.err.email": "Le courrier n'est pas valide",
    "register.err.email.required": "Le courrier est obligatoire",
    "regiter.err.password": "Le mot de passe n'est pas valide",
    "register.err.password.required": "Le mot de passe est obligatoire",
    "register.err.password.min":
      "Le mot de passe doit comporter au moins 8 caractères",
    "register.err.password_confirm": "La confirmation n'est pas valide",
    // notify
    "register.notify.success.title": "Correct",
    "register.notify.success.text": "Utilisateur enregistré avec succès",
    "register.notify.err.title": "Erreur",
    "register.notify.err.text": "Un utilisateur avec ce courrier existe déjà"
  }
};
