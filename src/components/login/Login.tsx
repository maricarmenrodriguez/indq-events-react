import React from "react";
import Card from "../../shared/components/commons/Card";
import { UserLoggedInConsumer } from "../../data/userLoggedIn/UserLoggedIn";
// import { FormattedMessage } from "react-intl";
import * as yup from "yup";
import { string } from "yup";
import useForm from "react-hook-form";

import {Link, navigate} from '@reach/router'
import { FormattedMessage } from "react-intl";

const registerSchema = yup.object().shape({
  email: string()
    .required("")
    .email(),
  password: string().required()
});

export default function Login() {
  const { register, getValues, errors, handleSubmit } = useForm({
    validationSchema: registerSchema
  });

  return (
    <Card title="indq events" subtitle="Iniciar sesión">
      <UserLoggedInConsumer>
        {value => {
          const onSubmit = (e: any) => {
            const { email, password } = getValues();
            if(value.login(email, password)){
              return navigate(`/eventlist`);
            }
          };

          return (
            <div className="flex justify-center text-center">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="m-5">
                  <label htmlFor="email"><FormattedMessage id="login.label.email" /></label> <br />
                  <input
                    name="email"
                    type="text"
                    className="border p-2 rounded w-full"
                    placeholder="email"
                    ref={register}
                  />
                </div>
                {errors.email && "El correo no es valido"}
                <div className="m-5">
                  <label htmlFor="password"><FormattedMessage id="login.label.password" /></label> <br />
                  <input
                    name="password"
                    type="password"
                    className="border p-2 rounded w-full"
                    placeholder="passsword"
                    ref={register}
                  />
                </div>
                {errors.password && "El password no es valido"}
                <div className="m-5">
                  <button
                    data-testid="confirm"
                    type="submit"
                    className="bg-blue-500 hover:bg-blue-600 w-full"
                  >
                    Acceder
                  </button>
                  <Link to="/register" className="text-blue-700 hover:underline">¿No tienes una cuenta? Registrate.</Link>
                </div>
              </form>
            </div>
          );
        }}
      </UserLoggedInConsumer>
    </Card>
  );
}
