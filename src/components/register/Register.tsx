import React, { useState } from "react";
import Card from "../../shared/components/commons/Card";
import useForm from "react-hook-form";
import ErrorInput from "../../shared/components/commons/ErrorInput";
import * as yup from "yup";
import { string } from "yup";
import Axios from "axios";
import { FormattedMessage } from "react-intl";
import Alert from "../../shared/components/commons/Alert";
import { injectIntl } from "react-intl";
import { Link } from "@reach/router";
import { api } from "../../global.json";

interface IData {
  firsName: string;
  lastName: string;
  email: string;
  password: string;
  password_confirm?: string;
  gender: string;
}

const SData = yup.object().shape({
  firstName: string()
    .required("register.err.firstName.required")
    .matches(/^[A-Za-z\s]+$/i, {
      excludeEmptyString: true,
      message: "register.err.firstName.matches"
    }),
  lastName: string()
    .required("register.err.lastName.required")
    .matches(/^[A-Za-z\s]+$/i, {
      excludeEmptyString: true,
      message: "register.err.firstName.matches"
    }),
  email: string()
    .email("register.err.email")
    .required("register.err.email.required"),
  password: string()
    .required("register.err.password.required")
    .min(8, "register.err.password.min"),
  password_confirm: string()
    .required()
    .min(8, "register.err.password_confirm"),
  gender: string()
    .default("male")
    .required()
});

function Register() {
  // console.log(<FormattedMessage id="register.swal.success.title" />);

  const [notify, setNotify] = useState({
    ok: false,
    title: {},
    text: {},
    type: ""
  });

  const { register, errors, setError, handleSubmit, reset } = useForm({
    mode: "onSubmit",
    defaultValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      password_confirm: "",
      gender: "male"
    },
    validationSchema: SData,
    submitFocusError: true
  });

  const onSubmit = async (data: any) => {
    setNotify({
      ok: false,
      title: {},
      text: {},
      type: ""
    });
    if (!validatePasswords(data.password, data.password_confirm)) return;
    console.log("Data a guardar: ", data);
    try {
      const response = await Axios.post(`${api}users`, data);
      console.log(response);
      setNotify({
        ok: true,
        title: <FormattedMessage id="register.notify.success.title" />,
        text: <FormattedMessage id="register.notify.success.text" />,
        type: "success"
      });
    } catch (error) {
      console.log(error);
      setNotify({
        ok: true,
        title: <FormattedMessage id="register.notify.err.title" />,
        text: <FormattedMessage id="register.notify.err.text" />,
        type: "error"
      });
    }
  };

  const validatePasswords = (password: string, confirm: string) => {
    if (password !== confirm) {
      setError("password_confirm", "noMacth", "La contraseña no coincide");
      return false;
    }
    return true;
  };

  return (
    <Card title="indq events" subtitle="Registro">
      {notify.ok ? <Alert {...notify} /> : null}

      <form onSubmit={handleSubmit(onSubmit)} data-testid="form">
        <div className="md:w-1/2 inline-block p-4">
          <label htmlFor="">
            <FormattedMessage id="register.firstName" />
            {errors.firstName && (
              <ErrorInput
                message={
                  <FormattedMessage
                    id={errors.firstName.message || "register.err.firstName"}
                  />
                }
              />
            )}
          </label>
          <input
            className="w-full"
            type="text"
            name="firstName"
            data-testid="name"
            ref={register}
          />
        </div>
        <div className="md:w-1/2 inline-block p-4">
          <label htmlFor="">
            <FormattedMessage id="register.lastName" />
            {errors.lastName && (
              <ErrorInput
                message={
                  <FormattedMessage
                    id={errors.lastName.message || "register.err.lastName"}
                  />
                }
              />
            )}
          </label>
          <input
            className="w-full"
            type="text"
            name="lastName"
            ref={register}
          />
        </div>

        <div className="md:w-1/2 inline-block p-4">
          <label htmlFor="">
            <FormattedMessage id="register.email" />
            {errors.email && (
              <ErrorInput
                message={
                  <FormattedMessage
                    id={errors.email.message || "register.err.email"}
                  />
                }
              />
            )}
          </label>
          <input className="w-full" type="text" name="email" ref={register} />
        </div>

        <div className="md:w-1/2 inline-block p-4">
          <label htmlFor="">
            <FormattedMessage id="register.gender" />
            {/* {errors.gender && (
              <ErrorInput
                message={<FormattedMessage id={errors.gender.message ||''} />}
              />
            )} */}
          </label>{" "}
          <br />
          <FormattedMessage id="register.gender.male" />
          <input
            type="radio"
            className="inline-block w-auto mr-5 ml-1"
            name="gender"
            value="male"
            ref={register}
          />
          <FormattedMessage id="register.gender.female" />
          <input
            type="radio"
            className="inline-block w-auto mr-5 ml-1"
            name="gender"
            value="male"
            ref={register}
          />
        </div>

        <div className="md:w-1/2 inline-block p-4">
          <label htmlFor="">
            <FormattedMessage id="register.password" />
            {errors.password && (
              <ErrorInput
                message={
                  <FormattedMessage
                    id={errors.password.message || "register.err.password"}
                  />
                }
              />
            )}
          </label>
          <input
            className="w-full"
            type="password"
            name="password"
            ref={register}
          />
        </div>

        <div className="md:w-1/2 inline-block p-4">
          <label htmlFor="">
            <FormattedMessage id="register.password_confirm" />
            {errors.password_confirm && (
              <ErrorInput
                message={
                  <FormattedMessage
                    id={
                      errors.password_confirm.message ||
                      "register.err.password_confirm"
                    }
                  />
                }
              />
            )}
          </label>
          <input
            className="w-full"
            type="password"
            name="password_confirm"
            ref={register}
          />
        </div>

        <div className="m-auto w-full text-center mt-5">
          <button
            data-testid="confirm"
            type="submit"
            className="bg-blue-500 hover:bg-blue-600  w-1/4 text-white mb-3"
          >
            <FormattedMessage id="register.btn.confirm" />
          </button>{" "}
          <br />
          <a href="#!" className="text-blue-700" onClick={reset}>
            <FormattedMessage id="register.btn.clean" />
          </a>
          <br />
          <Link to="/login" className="text-blue-700 font-bold">
            <FormattedMessage id="register.btn.login" />
          </Link>
        </div>
      </form>
    </Card>
  );
}

export default injectIntl(Register);
