import React, { useState, Fragment } from "react";
import useForm from "react-hook-form";
import * as yup from "yup";
import GoogleSuggest from "../../shared/components/commons/GoogleSuggest";
import MyMapComponent from "../pruebas/Maps";
import { UserLoggedInConsumer } from "../../data/userLoggedIn/UserLoggedIn";
import axios from "axios";
import { api } from "../../global.json";
import Alert from "../../shared/components/commons/Alert";
import moment from "moment";
import ErrorInput from "../../shared/components/commons/ErrorInput";
import { FormattedMessage } from "react-intl";

const today = moment().format("YYYY-MM-DD hh:mm:ss");

const SchemaRegister = yup.object().shape({
  title: yup.string().required(),
  description: yup.string().required(),
  date: yup.date().min(today, "La fecha no es valida")
});

export function RegisterEventContent({ userToken }) {
  const { register, getValues, errors, handleSubmit } = useForm({
    mode: "onSubmit",
    validationSchema: SchemaRegister,
    submitFocusError: true
  });

  const [lat, setLat] = useState(25.767191);
  const [lng, setLng] = useState(-108.992279);

  //Control de la imagen
  const [image, setImage] = useState();
  const [uploadingImage, setUploadingImage] = useState(false);
  const [fileNameImage, setFileNameImage] = useState("");

  async function saveImage() {
    const url = encodeURI(`${api}images`);
    setUploadingImage(true);
    let form = new FormData();
    form.append("file", image, image.name);
    try {
      const result = await axios.post(url, form, {
        headers: { Authorization: userToken }
      });
      setFileNameImage(result.data.fileName);
    } catch (error) {
      console.log(error);
      alert("Surgio un error al subir la imagen");
    }
  }

  function handleLocation(value) {
    setLat(parseFloat(value.geometry.location.lat()));
    setLng(parseFloat(value.geometry.location.lng()));
    console.log(lat, lng);
  }

  const onSubmit = async (data: any) => {
    if (!lat || !lng) {
      alert("Seleccione una ubicacion del evento");
    }
    const url = `${api}events`;
    try {
      let { title, description, date } = getValues();
      const data = {
        id: "",
        title,
        description,
        date,
        image: fileNameImage,
        attendances: 0,
        willYouAttend: false,
        location: [lng, lat]
      };
      console.log(data);
      let result = await axios.post(url, data, {
        headers: { Authorization: userToken }
      });
      console.log(result.data);
      alert("Evento guardado");
    } catch (error) {
      console.log(error);
      alert("Error al guardar el evento");
    }
  };

  const attendance = async(id:string) => {
    const url = encodeURI(`${api}events/attendance`);
    try {
      let result = await axios.post(url, {headers:{Authorization:userToken}});
      alert("Asistencia marcada")
    } catch (error) {
      console.log(error);
      alert("Ocurrio un error al enviar el mensaje");
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)} data-testid="form">
        <div className="flex">
          <div className="w-1/2 p-5">
            <div className="mb-5">
              <label className="text-xl">Titulo:</label>
              <input
                name="title"
                type="text"
                className="w-full"
                ref={register}
              />
              {errors.title && (
                <ErrorInput
                  message={
                    <FormattedMessage
                      id={
                        errors.title.message || "register.err.password_confirm"
                      }
                    />
                  }
                />
              )}
            </div>
            <div className="mb-5">
              <label className="text-xl">Descripción:</label>
              <textarea
                name="description"
                className="w-full border border-gray-500 rounded"
                rows={4}
                ref={register}
              ></textarea>
              {errors.description && (
                <ErrorInput
                  message={
                    <FormattedMessage
                      id={
                        errors.description.message ||
                        "register.err.password_confirm"
                      }
                    />
                  }
                />
              )}
            </div>
            <div className="mb-5">
              <label className="text-xl">Fecha:</label>
              <input
                name="date"
                type="datetime-local"
                className="w-full"
                ref={register}
                min={today}
              />
              {errors.date && <ErrorInput message={errors.date.message} />}
            </div>

            <div className="mb-5">
              <label className="text-xl mb-5">Imagen del evento:</label>
              {!uploadingImage && !fileNameImage ? (
                <Fragment>
                  <input
                    name="file"
                    type="file"
                    className="w-full"
                    onChange={e => {
                      setImage(e.target.files[0]);
                    }}
                  />
                  <div className="text-center mt-4">
                    <button
                      onClick={saveImage}
                      className="bg-blue-500 hover:bg-blue-700"
                    >
                      Guardar imagen
                    </button>
                  </div>
                </Fragment>
              ) : (
                <Alert title="Imagen guardada" duration={100} />
              )}
            </div>
          </div>
          <div className="w-1/2 p-5">
            <div className="mb-5">
              <label className="text-xl">Ubicación del evento</label>
              <GoogleSuggest setValue={handleLocation} />
            </div>
            <div className="mb-5">
              <MyMapComponent zoom={16} lat={lat} lng={lng} marker={true} />
            </div>
          </div>
        </div>

        <div className="text-center">
          <button className="bg-green-500 hover:bg-green-700 border-green-900 w-1/4 mt-20">
            Guardar evento
          </button>
        </div>
      </form>
    </div>
  );
}

export default function RegisterEvent() {
  return (
    <UserLoggedInConsumer>
      {value => {
        return <RegisterEventContent userToken={value.token} />;
      }}
    </UserLoggedInConsumer>
  );
}
