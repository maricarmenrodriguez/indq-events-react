import React, { FunctionComponent } from "react";
import Card from "../../shared/components/commons/Card";
import { api } from "../../global.json";
import MyMapComponent from "../pruebas/Maps";

import { IEvent } from "../../types";
import axios from "axios";
import { async } from "q";

interface props {
  eventData: IEvent;
}

const AnEvent: FunctionComponent<props> = ({ eventData }) => {


  
  function formatDate(date: string) {
    return `${date.substr(0, 10)} a las ${date.substr(11, 12)} horas.`;
  }

  

  return (
    <Card title={eventData.title}>
      <div className="md:flex flex-row">
        {eventData.image ? (
          <img
            src={`${api}images/${eventData.image}`}
            alt={eventData.image}
            className="sm:w-full md:w-1/2 p-5"
          />
        ) : null}
        <div className="sm:w-full md:w-1/2 p-5">
          <MyMapComponent
            zoom={16}
            lat={eventData.location[1]}
            lng={eventData.location[0]}
            marker={true}
          />
        </div>
      </div>
      <div className="p-5">
        <strong className="text-xl">{eventData.title}</strong>
        <p className="mt-5">
          <i className="fa fa-calendar fa-2x fa-lg mb-5"></i>
          &nbsp; <strong>Fecha del evento</strong>
          &nbsp; {formatDate(eventData.date)}
        </p>
        <p>{eventData.description}</p>
        <p> Asistencias {eventData.attendances}</p>
        {/* <p className="text-right">
          {eventData.willYouAttend ? (
            <button className="border border-red-600 hover:shadow text-black" onClick={()=>eventData.noAttended(eventData.id)}>
              No asistiré
            </button>
          ) : (
            <button
              className="border border-gray-600 hover:shadow text-black"
              onClick={()=>eventData.attended(eventData.id)}
            >
              Asistiré
            </button>
          )}
        </p> */}
      </div>
    </Card>
  );
};

export default AnEvent;
