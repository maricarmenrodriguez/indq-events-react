import React, { FunctionComponent, useState, useEffect } from "react";
import axios from "axios";
import { api } from "../../global.json";
import {
  UserLoggedInConsumer,
  IUserLogguedContext
} from "../../data/userLoggedIn/UserLoggedIn";
import { ScaleLoader } from "react-spinners";
import AnEvent from "../anEvent/AnEvent";
import { IEvent } from "../../types";
import GoogleSuggest from "../../shared/components/commons/GoogleSuggest";
import Alert from "../../shared/components/commons/Alert";

//socket
import openSocket from "socket.io-client";
const socket = openSocket("http://api.events.indqtech.com");

const EventListContent: FunctionComponent<IUserLogguedContext> = (
  props: IUserLogguedContext
) => {
  const [token, setToken] = useState("");
  const [loading, setLoading] = useState(true);
  const [events, setEvets] = useState([]);
  const [pages, setPages] = useState(1);
  const [page, setPage] = useState(1);
  const [searchTitle, setSearchTitle] = useState("");
  const [searchLocation, setSearchLocation] = useState([]);

  if (props.token && token === "") {
    setToken(props.token);
  }

  const getEvents = async () => {
    setLoading(true);
    const url = encodeURI(
      `${api}events?page=${page}${
        searchTitle !== "" ? "&title=" + searchTitle : ""
      }${
        searchLocation.length > 0
          ? "&lat=" + searchLocation[0] + "&lng=" + searchLocation[1]
          : ""
      }`
    );
    try {
      const response = await axios.get(url, {
        headers: { Authorization: token }
      });
      const { items, pages } = response.data;
      setPages(pages);
      setEvets(items);
      if (pages < page) {
        setPage(1);
      }
      setTimeout(() => {
        setLoading(false);
      }, 500);
    } catch (error) {
      console.log(error);
    }
  };

  async function attended(id) {
    const url = encodeURI(`${api}events/attendance/${id}`);
    try {
      const result = await axios.post(
        url,
        {},
        { headers: { Authorization: token } }
      );
      alert("Asistencia marcada");
      let newevents = events.map(e => {
        if (e.id === id) {
          e.attendances = result.data.attendance;
          e.willYouAttend = false;
        }
      });
      setEvets(newevents);
    } catch (error) {
      if (String(error).indexOf("403")) {
        return alert("Usted ya asistirá al este evento");
      }
      console.log("El error de la peticion es:", error);
      alert("Ocurrio un error al marcar la asistencia");
    }
  }

  async function noAttended(id) {
    const url = encodeURI(`${api}events/attendance/${id}`);

    try {
      const result = await axios.delete(url, {
        headers: { Authorization: token }
      });
      let newevents = events.map(e => {
        if (e.id === id) {
          e.attendances = result.data.attendance;
          e.willYouAttend = false;
        }
      });
      setEvets(newevents);
      alert("Asistencia desmarcada");
    } catch (error) {
      if (String(error).indexOf("403")) {
        return alert("Usted ya asistirá al este evento");
      }
      console.log("El error de la peticion es:", error);
      alert("Ocurrio un error al marcar la asistencia");
    }
  }

  const generatePagesConten = () => {
    let pagesContent = [];
    if (events.length <= 0) return false;
    for (let i = 1; i <= pages; i++) {
      pagesContent.push(
        <button
          className={`border hover:border-blue-700 bg-blue-400 text-white py-1 px-3 rounded m-2 ${
            page === i ? "bg-blue-700" : null
          }`}
          onClick={() => {
            setPage(i);
            getEvents();
          }}
          key={i}
        >
          {i}
        </button>
      );
    }
    return pagesContent;
  };

  function getContent() {
    if (loading) {
      return (
        <div className="flex justify-center mt-32">
          <ScaleLoader color="blue" loading={true} />
        </div>
      );
    } else {
      return (
        <div>
          <div id="Paginacion" className="text-center mt-10">
            {generatePagesConten()}
          </div>
          {events.length > 0
            ? events.map((event: IEvent) => (
                <div id="ContenidoGeneral" key={event.id}>
                  <div id="Listado" className="mt-5">
                    <AnEvent eventData={event} />
                  </div>
                </div>
              ))
            : null}
          <div id="Paginacion" className="text-center mt-10">
            {generatePagesConten()}
          </div>
        </div>
      );
    }
  }

  function handleSearchLocation(value) {
    setSearchLocation([
      value.geometry.location.lat(),
      value.geometry.location.lng()
    ]);
  }

  useEffect(() => {
    if (token !== "") {
      getEvents();
    }
  }, [page, token]);

  useEffect(() => {
    getEvents();
  }, [searchTitle, searchLocation]);

  socket.on("events", function(data) {
    console.log(events);
    let newEvent = events.map(e => {
      console.log(e);
      if (e.id === data.id) {
        e.attendances = data.attendances;
        console.log(e);
      }
      return e;
    });
    console.log(newEvent);
    setEvets([...newEvent]);
  });

  return (
    <div>
      <div className="w-ful mb-5l">
        <label htmlFor="search">Buscar por titulo del evento:</label>
        <input
          value={searchTitle === "" ? null : null}
          type="text"
          className="w-full mb-5"
          onChange={e => {
            setSearchTitle(e.target.value);
          }}
        />
        <label htmlFor="search">Buscar por ubicacion del evento:</label>
        <GoogleSuggest setValue={handleSearchLocation} />
      </div>
      <div className="mb-10 mt-5">
        {searchLocation.length > 0 ? (
          <a
            href="#!"
            className="text-blue-500 hover:text-blue-800 mt-10"
            onClick={() => {
              setSearchLocation([]);
            }}
          >
            {" "}
            X Busqueda por localización
          </a>
        ) : null}
      </div>
      {events.length > 0 ? (
        getContent()
      ) : (
        <Alert
          duration={10}
          title="No se encontraron eventos"
          type="warning"
        ></Alert>
      )}
    </div>
  );
};

//------------------------------------------------------------------------------
const EventsList: FunctionComponent = () => {
  return (
    <UserLoggedInConsumer>
      {value => {
        return <EventListContent {...value} />;
      }}
    </UserLoggedInConsumer>
  );
};

export default EventsList;
