import React from 'react'

import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const MyMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyCNo4HVxVlLxv6yMrkKeZPptH9zmhKkVBE",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(({lat,lng,zoom, marker}:any) =>(
  <GoogleMap
    defaultZoom={zoom}
    defaultCenter={{ lat: lat, lng: lng }}
    center={{ lat: lat, lng: lng }}
  >
    {marker && <Marker position={{ lat: lat, lng: lng }} />}
  </GoogleMap>
))

export default MyMapComponent