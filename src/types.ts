export interface IEvent {
    attendances: number;
    date: string;
    description: string;
    id: string;
    image: string;
    location: Array<number>;
    title: string;
    willYouAttend: boolean;
  }
  