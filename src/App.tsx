import React, { Fragment } from "react";
//Components
import Header from "./shared/components/main/Header";
import Login from "./components/login/Login";

//Router
import { Router, Redirect } from "@reach/router";
import Route from "./shared/components/main/Route";

//Data
import MyIntlProvider, {
  IntlConsumer
} from "./data/intl/context/IntelProvider";
import { IntlProvider } from "react-intl";
import UserLoggedInProvider from "./data/userLoggedIn/UserLoggedIn";
import Register from "./components/register/Register";
import EventsList from "./components/eventslist/EventsList";
import Footer from "./shared/components/main/Footer";
import RegisterEvent from "./components/registerEvent/RegisterEvent";

const App: React.FC = () => {
  return (
    <Fragment>
      <UserLoggedInProvider>
        <MyIntlProvider>
          <IntlConsumer>
            {values => {
              return (
                <IntlProvider locale={values.locale} messages={values.messages}>
                  <Header />
                  <div className="container mx-auto mt-10">
                    <Router>
                      <Route component={Login} path="/login"></Route>
                      <Route component={Register} path="/register"></Route>
                      <Route component={EventsList} path="/eventlist"></Route>
                      <Route
                        component={RegisterEvent}
                        path="/registerevent"
                      ></Route>
                      <Route component={EventsList} path="/"></Route>
                    </Router>
                  </div>
                  <Footer />
                </IntlProvider>
              );
            }}
          </IntlConsumer>
        </MyIntlProvider>
      </UserLoggedInProvider>
    </Fragment>
  );
};

export default App;
